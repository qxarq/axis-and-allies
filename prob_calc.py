import numpy as np


class Unit:
    def __init__(self, IPC, attack, defense):
        self.IPC = IPC
        self.attack = attack
        self.defense = defense


class Army:
    def __init__(self, army_comp):
        self.infantry = army_comp[0]
        self.artillery = army_comp[1]
        self.mech_infantry = army_comp[2]
        self.tank = army_comp[3]
        self.t_bomber = army_comp[4]
        self.fighter = army_comp[5]
        self.s_bomber = army_comp[6]
        self.att = ['infantry', 'artillery', 'mech_infantry', 'tank', 't_bomber', 'fighter', 's_bomber']

    @property
    def getIPC(self):
        totIPC = 0
        for a in self.att:
           totIPC += getattr(self, a) * eval(a).IPC
        return totIPC

    @property
    def totUnits(self):
        totUnits = 0
        for a in self.att:
            totUnits += getattr(self, a)
        return totUnits

    @property
    def comp(self):
        composition = np.int_(np.zeros(7))
        for index, unit in enumerate(self.att):
            composition[index] = getattr(self, unit)
        return composition


class Navy:
    def __init__(self, army_comp):
        self.t_bomber = army_comp[0]
        self.fighter = army_comp[1]
        self.s_bomber = army_comp[2]
        self.battleship = army_comp[3]
        self.battleship_d = army_comp[4]
        self.carrier = army_comp[5]
        self.carrier_d = army_comp[6]
        self.cruiser = army_comp[7]
        self.destroyer = army_comp[8]
        self.sub = army_comp[9]
        self.att = ['t_bomber', 'fighter', 's_bomber', 'battleship', 'battleship_d', 'carrier', 'carrier_d', 'cruiser', 'destroyer', 'sub']

    @property
    def getIPC(self):
        totIPC = 0
        for a in self.att:
           totIPC += getattr(self, a) * eval(a).IPC
        return totIPC

    @property
    def totUnits(self):
        totUnits = 0
        for a in self.att:
            totUnits += getattr(self, a)
        return totUnits

    @property
    def comp(self):
        composition = np.int_(np.zeros(10))
        for index, unit in enumerate(self.att):
            composition[index] = getattr(self, unit)
        return composition


infantry = Unit(3, 1/6, 2/6)
artillery = Unit(4, 2/6, 2/6)
mech_infantry = Unit(4, 1/6, 2/6)
tank = Unit(6, 3/6, 3/6)
t_bomber = Unit(11, 3/6, 3/6)
fighter = Unit(10, 3/6, 4/6)
s_bomber = Unit(12, 4/6, 1/6)
battleship = Unit(20, 4/6, 4/6)
battleship_d = Unit(20, 4/6, 4/6)
carrier = Unit(16, 0/6 , 2/6)
carrier_d = Unit(16, 0/6, 2/6)
cruiser = Unit(12, 3/6, 3/6)
destroyer = Unit(8, 2/6, 2/6)
sub = Unit(6, 2/6, 1/6)


def get_comp(army):
    for a in army.att:
        print('\t', a, getattr(army, a))


def round_sim(attacker, defender):
    """
    Calculate the number of hits scored by an attacker and defender respectively.
    This takes into account assistance by artillery, tanks and fighters to infantry, mechanized infantry, and tactical bombers.

    :param attacker: information about the attacker
    :param defender: information about the defender
    :return: A tuple containing the number of hits scored by the attacker and defender respectively
    """
    if type(attacker) == Army:
        max_t_f_assist = getattr(attacker, 'tank') + getattr(attacker, 'fighter')
        max_art_assist = getattr(attacker, 'artillery')
    elif type(attacker) == Navy:
        max_t_f_assist = getattr(attacker, 'fighter') 
    t_f_assist = 0
    art_assist = 0
    hits_atk = 0
    for unit in attacker.att:
        if unit == 'mech_infantry' or unit == 'infantry':
            art_assist_now = min(getattr(attacker, unit), max_art_assist - art_assist)
            hits_atk += np.sum(np.random.random(art_assist_now) < eval(unit).attack + 1/6)
            hits_atk += np.sum(np.random.random(getattr(attacker, unit) - art_assist_now) < eval(unit).attack)
            art_assist += art_assist_now
        if unit == 't_bomber':
            if type(attacker) == Army:
                t_f_assist = min(getattr(attacker, unit), getattr(attacker, 'tank') + getattr(attacker, 'fighter'))
            elif type(attacker) == Navy:
                t_f_assist = min(getattr(attacker, unit), getattr(attacker, 'fighter'))
            hits_atk += np.sum(np.random.random(t_f_assist) < eval(unit).attack + 1/6)
            hits_atk += np.sum(np.random.random(getattr(attacker, unit) - t_f_assist) < eval(unit).attack)
        else:
            hits_atk += np.sum(np.random.random(getattr(attacker, unit)) < eval(unit).attack)
    # Feedback
    #    print(hits_atk, 'total hits were scored after', unit)
    # print(art_assist, 'Artillery assisted infantry\n', t_f_assist, 'fighter(s) or tank(s) assisted tactical bombers')

    hits_def = 0
    for unit in defender.att:
        hits_def += np.sum(np.random.random(getattr(defender, unit)) < eval(unit).defense)
    # Feedback
    # print('Attacker scored', hits_atk, 'hits\n', 'Defender scored', hits_def, 'hits')
    return (hits_atk, hits_def)


def round_one_sim(attacker, defender):
    if type(attacker) == Army:
        max_t_f_assist = getattr(attacker, 'tank') + getattr(attacker, 'fighter')
        max_art_assist = getattr(attacker, 'artillery')
    elif type(attacker) == Navy:
        max_t_f_assist = getattr(attacker, 'fighter') 
    t_f_assist = 0
    art_assist = 0
    hits_atk = 0
    for unit in attacker.att:
        if unit == 'mech_infantry' or unit == 'infantry':
            art_assist_now = min(getattr(attacker, unit), max_art_assist - art_assist)
            hits_atk += np.sum(np.random.random(art_assist_now) < eval(unit).attack + 1/6)
            hits_atk += np.sum(np.random.random(getattr(attacker, unit) - art_assist_now) < eval(unit).attack)
            art_assist += art_assist_now
        if unit == 't_bomber':
            if type(attacker) == Army:
                t_f_assist = min(getattr(attacker, unit), getattr(attacker, 'tank') + getattr(attacker, 'fighter'))
            elif type(attacker) == Navy:
                t_f_assist = min(getattr(attacker, unit), getattr(attacker, 'fighter'))
            hits_atk += np.sum(np.random.random(t_f_assist) < eval(unit).attack + 1/6)
            hits_atk += np.sum(np.random.random(getattr(attacker, unit) - t_f_assist) < eval(unit).attack)
        else:
            hits_atk += np.sum(np.random.random(getattr(attacker, unit)) < eval(unit).attack)
    #Feedback
    #    print(hits_atk, 'total hits were scored after', unit)
    #print(art_assist, 'Artillery assisted infantry\n', t_f_assist, 'fighter(s) or tank(s) assisted tactical bombers')

    hits_def = 0
    for unit in defender.att:
        hits_def += np.sum(np.random.random(getattr(defender, unit)) < eval(unit).defense)
    #Feedback
    #print('Attacker scored', hits_atk, 'hits\n', 'Defender scored', hits_def, 'hits')
    return (hits_atk, hits_def)


def assign_hits_atk_land(hits, army):
    """
    Assigns hits to attackers on land. Prioritizes taking hits in the following order:
       1: If there's mech_infantry, take all hits to infantry first, then to mech_infantry until there is just one left.
          Otherwise, spare one infantry
       2: Take hits to artillery
       3: Take hits to tanks
       4: Take hits to fighters if there are more fighters than tactical bombers, or if there are the same amount
       5: Take hits to tactical bombers
       6: Take hits to strategic bombers
    :param hits: the hits to be assigned
    :param army: the army to which to assign hits
    :return: the army after which hits have been assigned
    """
    if hits >= army.totUnits:
        army = Army(np.int_(np.zeros(7)))
    else: 
        while hits > 0:
            if army.infantry + army.mech_infantry > 1:
                if army.infantry > 0 and army.mech_infantry > 0:
                    army.infantry, hits = army.infantry-1, hits-1
                elif army.infantry > 0 and army.mech_infantry == 0:
                    army.infantry, hits = army.infantry-1, hits-1
                elif army.infantry == 0 and mech_infantry > 0:
                    army.mech_infantry, hits = army.mech_infantry-1, hits-1
            elif army.artillery > 0:
                army.artillery, hits = army.artillery-1, hits-1
            elif army.tank > 0:
                army.tank, hits = army.tank-1, hits-1
            elif army.fighter + army.t_bomber > 0:
                if army.fighter > army.t_bomber:
                    army.fighter, hits = army.fighter-1, hits-1
                elif army.fighter == army.t_bomber:
                    army.fighter, hits = army.fighter-1, hits-1
                else:
                    army.t_bomber, hits = army.t_bomber-1, hits-1
            elif army.s_bomber > 0:
                army.s_bomber, hits = army.s_bomber-1, hits-1
    return army

def assign_hits_def_land(hits, army):
    """
    Assigns hits to defenders on land. Prioritizes taking hits in the following order:
       1: Take hits to strategic bombers (This is placed here to give the best possible outcome for winning.
          Though they are bad on defense, taking hits to bombers is strongly negative in IPC,
          so depending on the battle, it could make more sense to take hits to bombers last.
       2: Take hits to infantry
       3: Take hits to mechanized infantry
       4: Take hits to artillery
       5: Take hits to tanks
       6: Take hits to tactical bombers
       7: Take hits to fighters. Taking hits to tactical bombers before fighters is more negative in IPC,
          but fighters defend better.

    :param hits: the hits to be assigned
    :param army: the army to which to assign hits
    :return: the army after which hits have been assigned
    """
    if hits >= army.totUnits:
        army = Army(np.int_(np.zeros(7)))
    else:
        while hits > 0:
            if army.s_bomber > 0:
                army.s_bomber, hits = army.s_bomber-1, hits-1
            elif army.infantry > 0:
                army.infantry, hits = army.infantry-1, hits-1
            elif army.mech_infantry > 0:
                army.mech_infantry, hits = army.mech_infantry-1, hits-1
            elif army.artillery > 0:
                army.artillery, hits = army.artillery-1, hits-1
            elif army.tank > 0:
                army.tanks, hits = army.tanks-1, hits-1
            elif army.t_bomber > 0:
                army.t_bomber, hits = army.t_bomber-1, hits-1
            elif army.fighter > 0:
                army.fighter, hits = army.fighter-1, hits-1
    return army


def assign_hits_atk_sea(hits, navy):
    """
    Assigns hits to attackers on sea. Prioritizes taking hits in the following order:
       1: Take hits to undamaged battleships
       2: Take hits to undamaged carriers if there are no fighters or bombers
       3: Take hits to fighters and tactical bombers if there are no carriers.
          Prioritize keeping the same number of fighters as tactical bombers
       4: Take hits to cruisers
       5: Take hits to remaining fighters
       6: Take hits to strategic bombers
       7: Take hits to damaged carriers
       8: Take hits to damaged battleships
    :param hits: the hits to be assigned
    :param navy: the navy to which to assign hits
    :return: the navy after which hits have been assigned
    """
    if hits >= navy.totUnits + navy.battleship + navy.carrier:
        navy = Navy(np.int_(np.zeros(10)))
    else:
        while hits > 0:
            if navy.battleship > 0:
                navy.battleship, navy.battleship_d, hits = navy.battleship-1, navy.battleship_d+1, hits-1
            elif navy.carrier > 0 and navy.fighter + navy.t_bomber == 0:
                navy.carrier, navy.carrier_d, hits = navy.carrier-1, navy.carrier_d+1, hits-1
            elif navy.sub > 0:
                navy.sub, hits = navy.sub-1, hits-1
            elif navy.destroyer > 0:
                navy.destroyer, hits = navy.destroyer-1, hits-1
            elif navy.carrier == 0 and (navy.fighter + navy.t_bomber > 0):
                if navy.fighter > navy.t_bomber:
                    navy.fighter, hits = navy.fighter-1, hits-1
                elif navy.fighter == navy.t_bomber:
                    navy.fighter, hits = navy.fighter-1, hits-1
                else:
                    navy.t_bomber, hits = navy.t_bomber-1, hits-1
            elif navy.cruiser > 0:
                navy.cruiser, hits = navy.cruiser-1, hits-1
            elif navy.fighter + navy.t_bomber > 0:
                if navy.fighter > navy.t_bomber:
                    navy.fighter, hits = navy.fighter-1, hits-1
                elif navy.fighter == navy.t_bomber:
                    navy.fighter, hits = navy.fighter-1, hits-1
                else:
                    navy.t_bomber, hits = navy.t_bomber-1, hits-1
            elif navy.s_bomber > 0:
                navy.s_bomber, hits = navy.s_bomber-1, hits-1
            elif navy.carrier_d > 0:
                navy.carrier_d, hits = navy.carrier_d-1, hits-1
            else:
                navy.battleship_d, hits = navy.battleship_d-1, hits-1
    return navy


def assign_hits_def_sea(hits, navy):
    if hits >= navy.totUnits + navy.battleship + navy.carrier:
        navy = Navy(np.int_(np.zeros(10)))
    else:
        while hits > 0:
            if navy.battleship > 0:
                navy.battleship, navy.battleship_d, hits = navy.battleship-1, navy.battleship_d+1, hits-1
            elif navy.carrier > 0 and navy.fighter + navy.t_bomber == 0:
                navy.carrier, navy.carrier_d, hits = navy.carrier-1, navy.carrier_d+1, hits-1
            elif navy.sub > 0:
                navy.sub, hits = navy.sub-1, hits-1
            elif navy.destroyer > 0:
                navy.destroyer, hits = navy.destroyer-1, hits-1
            elif navy.carrier == 0 and (navy.fighter + navy.t_bomber > 0):
                if navy.t_bomber > 0:
                    navy.t_bomber, hits = navy.t_bomber-1, hits-1
                else:
                    navy.fighter, hits = navy.fighter-1, hits-1
            elif navy.cruiser > 0:
                navy.cruiser, hits = navy.cruiser-1, hits-1
            elif navy.fighter + navy.t_bomber > 0:
                if navy.t_bomber > 0:
                    navy.t_bomber, hits = navy.t_bomber-1, hits-1
                else:
                    navy.fighter, hits = navy.fighter-1, hits-1
            elif navy.carrier_d > 0:
                navy.carrier_d, hits = navy.carrier_d-1, hits-1
            else:
                navy.battleship_d, hits = navy.battleship_d-1, hits-1
    return navy


def battle_sim(attacker, defender):
    feedback = 0
    rounds = 0
    total_hits = np.array([0, 0])
    if type(attacker) == Army and type(defender) == Army:
        attacker_new, defender_new = Army(attacker.comp), Army(defender.comp)
        if feedback == 1: print('LAND BATTLE')
        while (attacker_new.totUnits > 0 and defender_new.totUnits > 0) and rounds < 1000:
            rounds += 1
            hits = round_sim(attacker_new, defender_new)
            attacker_new = assign_hits_atk_land(hits[1], attacker_new)
            defender_new = assign_hits_def_land(hits[0], defender_new)
            total_hits += np.array(hits)
            if feedback == 1: print('round', str(rounds)+': Attacker', hits[0], '|', 'Defender', hits[1])
        if feedback == 1: print('Total Hits: Attacker', total_hits[0], 'Defender', total_hits[1])
    
    elif type(attacker) == Navy and type(defender) == Navy:
        attacker_new, defender_new = Navy(attacker.comp), Navy(defender.comp)
        if feedback == 1: print('SEA BATTLE')
        while (attacker_new.totUnits > 0 and defender_new.totUnits > 0) and rounds < 1000:
            rounds += 1
            hits = round_sim(attacker_new, defender_new)
            attacker_new = assign_hits_atk_sea(hits[1], attacker_new)
            defender_new = assign_hits_def_sea(hits[0], defender_new)
            total_hits += np.array(hits)
            if feedback == 1: print('round', str(rounds)+': Attacker', hits[0], '|', 'Defender', hits[1])
        if feedback == 1: print('Total Hits: Attacker', total_hits[0], 'Defender', total_hits[1])

    else:
        print('These armies/navies need to both be of the same type. Attacker is of type', type(attacker_new), 'and defender is of type', type(defender_new))
    if attacker_new.totUnits == 0:
        winner = 0
    else:
        winner = 1
    return winner, total_hits[0], total_hits[1], attacker.comp - attacker_new.comp, defender.comp - defender_new.comp


def battle_stat(attacker, defender):
    simulations = 1000
    atk_wins, atk_hits, def_hits = 0, 0, 0
    if type(attacker) != type(defender):
        print('These armies/navies need to both be of the same type. Attacker is of type', type(attacker_new), 'and defender is of type', type(defender_new))
    else:
        if type(attacker) == Army and type(defender) == Army:
            atk_losses, def_losses = np.int_(np.zeros(7)), np.int_(np.zeros(7))
        elif type(attacker) == Navy and type(defender) == Navy:
            atk_losses, def_losses = np.int_(np.zeros(10)), np.int_(np.zeros(10))
        stats = np.array([atk_wins, atk_hits, def_hits, atk_losses, def_losses])
        for i in range(simulations):
            battlesim = np.array(battle_sim(attacker, defender))
            stats += battlesim
    print('Simulations:', simulations)
    print('Avg Attacker Victory:', str(round(stats[0] * 100 / simulations, 2))+'%')
    print('Avg Defender Victory:', str(round(100 - (stats[0] * 100 / simulations), 2))+'%')
    print('Avg Attacker Hits:', round(stats[1] / simulations, 2))
    print('Avg Defender Hits:', round(stats[2] / simulations, 2))
    avg_atk_loss = np.round(stats[3].astype(float) / simulations, 2)
    avg_def_loss = np.round(stats[4].astype(float) / simulations, 2)
    avg_atk_rem = np.round(attacker.comp - avg_atk_loss, 2)
    avg_def_rem = np.round(defender.comp - avg_def_loss, 2)
    if type(attacker) == Army and type(defender) == Army:
        avg_atk_loss_army = Army(avg_atk_loss)
        avg_def_loss_army = Army(avg_def_loss)
        avg_atk_rem_army = Army(avg_atk_rem)
        avg_def_rem_army = Army(avg_def_rem)
    elif type(attacker) == Navy and type(defender) == Navy:
        avg_atk_loss_army = Navy(avg_atk_loss)
        avg_def_loss_army = Navy(avg_def_loss)
        avg_atk_rem_army = Navy(avg_atk_rem)
        avg_def_rem_army = Navy(avg_def_rem)
    print('Avg Attacker Losses:', round(avg_atk_loss_army.getIPC, 2), 'IPC')
    get_comp(avg_atk_loss_army)
    print('Avg Defender Losses:', round(avg_def_loss_army.getIPC, 2), 'IPC')
    get_comp(avg_def_loss_army)
    print('Avg Attacker Remaining:')
    get_comp(avg_atk_rem_army)
    print('Avg Defender Remaining:')
    get_comp(avg_def_rem_army)
    #print(stats)


axis_army = Army(np.int_(np.zeros(7)))
allies_army = Army(np.int_(np.zeros(7)))
axis_navy = Navy(np.int_(np.zeros(10)))
allies_navy = Navy(np.int_(np.zeros(10)))

# Individual Test Values
axis_army.infantry = 10
axis_army.artillery = 5
axis_army.fighter = 2
axis_army.t_bomber = 0
allies_army.infantry = 10
allies_army.fighter = 3

axis_navy.battleship = 2
axis_navy.destroyer = 3
axis_navy.sub = 5
allies_navy.sub = 3
allies_navy.battleship = 1
allies_navy.cruiser = 4
allies_navy.carrier = 1
allies_navy.fighter = 1
allies_navy.t_bomber = 1
