import numpy as np

class Unit:
    def __init__(self, IPC, attack, defense, hits, assist):
        self.IPC = IPC
        self.attack = attack
        self.defense = defense
        self.hits = hits
        self.assist = assist


class Army:
    def __init__(self, army_comp):
        self.infantry = army_comp[0]
        self.artillery = army_comp[1]
        self.mech_infantry = army_comp[2]
        self.tank = army_comp[3]
        self.t_bomber = army_comp[4]
        self.fighter = army_comp[5]
        self.s_bomber = army_comp[6]
        self.battleship = army_comp[7]
        self.carrier = army_comp[8]
        self.cruiser = army_comp[9]
        self.destroyer = army_comp[10]
        self.sub = army_comp[11]
        self.att = ['mech_infantry', 'artillery', 'infantry', 'tank', 't_bomber', 'fighter', 's_bomber', 'battleship', 'carrier', 'cruiser', 'destroyer', 'sub']

    @property
    def getIPC(self):
        totIPC = 0
        for a in self.att:
           totIPC += getattr(self, a) * eval(a).IPC
        return totIPC


infantry = Unit(3, 1/6, 2/6, 1, [])
artillery = Unit(4, 2/6, 2/6, 1, ['mech_infantry', 'infantry'])
mech_infantry = Unit(4, 1/6, 2/6, 1, [])
tank = Unit(6, 3/6, 3/6, 1, ['t_bomber'])
t_bomber = Unit(11, 3/6, 3/6, 1, [])
fighter = Unit(10, 3/6, 4/6, 1, ['t_bomber'])
s_bomber = Unit(12, 4/6, 1/6, 1, [])
battleship = Unit(20, 4/6, 4/6, 2, [])
carrier = Unit(16, 0/6 , 2/6, 2, [])
cruiser = Unit(12, 3/6, 3/6, 1, [])
destroyer = Unit(8, 2/6, 2/6, 1, [])
sub = Unit(6, 2/6, 1/6, 1, [])


def get_comp(army):
    for a in army.att:
        print(a, getattr(army, a))


def round_sim(attacker, defender):
    max_t_f_assist = getattr(attacker, 'tank') + getattr(attacker, 'fighter')
    max_art_assist = getattr(attacker, 'artillery')
    t_f_assist_rem = max_t_f_assist
    art_assist_rem = max_art_assist
    t_f_assist = 0
    art_assist = 0
    hits_atk = 0
    for unit in attacker.att:
        if unit == 'mech_infantry' or 'infantry':
            art_assist_now = min(getattr(attacker, unit), max_art_assist - art_assist)
            #if getattr(attacker, unit) > art_assist_rem:
            #    art_assist += art_assist_rem
            #    art_assist_rem = 0
            #else:
            #    art_assist += getattr(attacker, unit)
            #    art_assist_rem -= getattr(attacker, unit)
            hits_atk += np.sum(np.random.random(art_assist) < eval(unit).attack + 1/6)
            hits_atk += np.sum(np.random.random(getattr(attacker, unit) - art_assist_now) < eval(unit).attack)
            art_assist += art_assist_now
        if unit == 't_bomber':
            if getattr(attacker, unit) > t_f_assist_rem:
                t_f_assist += t_f_assist_rem
                t_f_assist_rem = 0
            else:
                t_f_assist += getattr(attacker, unit)
                t_f_assist_rem -= getattr(attacker, unit)
            hits_atk += np.sum(np.random.random(art_assist) < eval(unit).attack + 1/6)
            hits_atk += np.sum(np.random.random(getattr(attacker, unit) - art_assist) < eval(unit).attack)
        else:
            hits_atk += np.sum(np.random.random(getattr(attacker, unit)) < eval(unit).attack)
        print(art_assist, 'Artillery assisted infantry\n', t_f_assist, 'fighters or tanks assisted tactical bombers')

    hits_def = 0
    for unit in defender.att:
        hits_def += np.sum(np.random.random(getattr(defender, unit)) < eval(unit).defense)
    print('Attacker scored', hits_atk, 'hits\n', 'Defender scored', hits_def, 'hits')
    return (hits_atk, hits_def)


axis = Army(np.int_(np.zeros(12)))
allies = Army(np.int_(np.zeros(12)))

#Individual Test Values
axis.infantry = 10
axis.artillery = 2
allies.infantry = 10
